<?php

class Html{
	private $layout = 'html/layout.html';
	private $basePath = 'html/';
	
	public function buildPage($site, $content){
		$userinfo = '';		
		if(isset($_SESSION['user']['username'])){
			$userinfo = 'Eingeloggt als: <a href="index.php?action=profile">' . $_SESSION['user']['username'] .'</a>';	
			$userinfo .= '<form action="index.php?action=logout" method="POST"><input type="submit" value="Logout" name="logout" /></form>';
		}


		$html = file_get_contents($this->layout);
		$search = array('{{USERNAME}}',
						'{{SITE}}',
						'{{NAVIGATION}}',
						'{{CONTENT}}',
					);
		
		$replace = array($userinfo,
						$site,
						$this->buildNavigation(),
						$content,
					);
		
		return str_replace($search, $replace, $html);
	}
	
	public function getPage($page){
		return file_get_contents($this->basePath . $page . '.html');
	}

	private function buildNavigation(){
		$nav = '<td><a href="index.php?action=index">Home</a></td>';
		//nicht eingeloggt --> register &login  in der Navigation anzeigen
		if(!isset($_SESSION['user']['username'])){
			$nav .= '<td><a href="index.php?action=login">Login</a></td>
					<td><a href="index.php?action=register">Register</a></td>';		
		}
		$nav .= '<td><a href="index.php?action=about">about</a></td></tr>';

		return $nav;
	}
}

?>
