<?php

	// JUST FOR DEV!
	error_reporting(E_ALL);
	ini_set('display_errors', TRUE);

	session_start();
	require_once('include.php');

	$c = new Controller();
	if (isset($_GET['action'])){
		$c->runAction($_GET['action']);
	}else{
		$c->runAction();
	}

?>
