<?php

class Controller{
	private $html = null;
	
	public function __construct(){
		$this->html = new Html();
	}
	
	public function runAction($action='index'){
		if($action == 'index'){
			$this->actionIndex();
		}elseif($action == 'register'){
			$this->actionRegister();
		}elseif($action == 'login'){
			$this->actionLogin();
		}elseif($action == 'about'){
			$this->actionAbout();
		}elseif($action =='editEntry'){
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				$this->actionEditEntry($id);
			}else{
				$this->actionError('Fehlende Paramater.');
			}
		}elseif($action =='deleteEntry'){
			if(isset($_GET['id'])){
				$id = (int)$_GET['id'];
				$this->actionDeleteEntry($id);
			}else{
				$this->actionError('Fehlende Parameter.');
			}
		}elseif($action =='profile'){
			if(isset($_SESSION['user']['username']))
				$this->actionProfile();
			else
				$this->actionError('Aufgerufen Action existiert nicht.');
		}elseif($action == 'logout'){
			$this->actionLogout();
		}else{
			
		}
	}
	
	public function actionError($error){
		$content = $this->html->getPage('error');
		$content = str_replace('{{DETAIL_MSG}}', $error, $content);
		echo $this->html->buildPage('Error', $content, 'error');
		die();
	}

	private function actionIndex($message = NULL){
		$entries = '';
		$actionfield = '';
		
		// neuer Eintrag erfasst
		if(isset($_POST['submitEntry'])){
			$entry = new Entry();
			//post data auslesen
			$text = $this->escape($_POST['text']);
			// validieren
			$v = new Validator();
			if(!$v->isString($text, 1, 8000))
				$message = '<div class="flash-error">Gästebuch Eintrag ungültig</div>';
			
			// aktuellen User auslesen
			$user = User::model()->findByAttributes(array('username' => $_SESSION['user']['username'] ));
			$arg = array('id_user' => $user->id, 'message'=> $text);
			$entry->populate($arg);
			$entry->save();
			$message ='<div class="flash-success">Eintrag erfolgreich gespeichert.</div>';			
		}

		$actionfield = '<div class="flash-notice">
					Damit Sie Einträge erfassen können müssen Sie eingeloggt sein.<br />
					Loggen Sie sich <a href="index.php?action=login"> hier</a> ein. <br />
					Sie haben noch kein Konto? Dann registrieren Sie sich <a href="index.php?action=register"> hier</a>. </div>';

		if(isset($_SESSION['user']['username'])){
			$actionfield = '<h4>Neuen Eintrag erfassen:</h4>
			<form action="index.php?action=index" method="POST">
			<textarea cols="60" rows="10" wrap="physical" name="text"></textarea>
			<br /><input type="submit" value="eintragen" name="submitEntry" id="submitEntry" /></form>';
		}


		// alle Gästebuch Einträge auslesen und anzeigen
		// eigene erkennen und diese bearbeitbar / löschbar
		// admin alle löschbar
		$output = '';
		$entries = Entry::model()->findAll();
		$actual_id = '';
		$isAdmin = false;

		if(isset($_SESSION['user']['username'])){
			$actual_user = User::model()->findByAttributes(array('username' => $_SESSION['user']['username'] ));
			$actual_id = $actual_user->id;
			$isAdmin = $actual_user->isAdmin;
		}

		if($entries == null){
			$output = 'Leider keine Einträge vorhanden';
		}else{
			foreach($entries as $entry){
				$user = User::model()->findByPk($entry->id_user);
				$output .= '<div class="entries" id="e'. $entry->id .'">';
				// nur eigene Einträge lösch/ editierbar
				if($entry->id_user == $actual_id || $isAdmin){
					$output .= '<div class="actionIcons"><a href="index.php?action=deleteEntry&id='.$entry->id.'"><img src="./images/delete.png" /></a>&nbsp;';
					$output .= '<a href="index.php?action=editEntry&id='.$entry->id.'"><img src="./images/edit.png" /></a> </div>';
				}
				$output .= '<p>'.$entry->time.'</p>';
				$output .=	'<p>'. $user->username . ' ('. $user->email .') </p>';
				$output .= '<p>'.nl2br(htmlspecialchars($entry->message)) .'</p>';
				$output .= '</div>';
			}
		}	

		$content = $this->html->getPage('home');
		$search = array('{{MESSAGE}}', '{{ACTION_FIELD}}', '{{ENTRIES}}');
		$replace = array($message, $actionfield, $output);
		$content = str_replace($search, $replace, $content);
		echo $this->html->buildPage('Home', $content);
	}

	private function actionEditEntry($id){	
		$output = '';
		$message = '';

		$entry = Entry::model()->findByPk($id);
		if($entry != null){
			$output .= '<form action="index.php?action=editEntry&id='.$entry->id.'" method="POST">';
			$output .= '<textarea cols="60" rows="10" wrap="physical" name="editText">'.$entry->message.'</textarea>';
			$output .= ' <br /> <input type="submit" value="speichern und zurück" name="submitEdit" id="submitEdit" /> </form> ';
		}else{
			$output = '<div class="flash-error">Der gewünschte Eintrag existiert nicht mehr!</div>';
		}

		if(isset($_POST['submitEdit'])){
			$text = $this->escape($_POST['editText']);
			// validieren
			$v = new Validator();
			if(!$v->isString($text, 1, 8000)){
				$message = '<div class="flash-error">Eintrag ungültig</div>';
			}else{
				$entry->message = $text;
				$entry->save();
				$message = '<div class="flash-success">Der Eintrag wurde erfolgreich geändert.</div>';
				$this->actionIndex($message);
				return;
			}		
		}

		$content = $this->html->getPage('edit');
		$content = str_replace('{{EDIT}}', $output, $content);
		$content = str_replace('{{MESSAGE}}', $message, $content);
		echo $this->html->buildPage('Eintrag bearbeiten', $content);
	}

	private function actionDeleteEntry($id){
		$entry = Entry::model()->findByPk($id);
		if($entry == null)
			$this->actionError('Eintrag existiert nicht mehr.');

		$message ='';
		$user = User::model()->findByAttributes(array('username' => $_SESSION['user']['username'] ));
		if($entry->id_user != $user->id){
			$message = 'Eintrag konnte nicht gelöscht werden. Zugriff verweigert';
		}else{
			if($entry->delete())
				$message = '<div class="flash-success">Der Eintrag wurde erfolgreich gelöscht.</div>';
			else
				$this->actionError('Fehler beim löschen des Eintrages.');			
		}

		$this->actionIndex($message);
		return;
	}
	
	private function actionProfile(){
		$message = '';
		// aktueller User auslesen
		$user = User::model()->findByAttributes(array('username' => $_SESSION['user']['username'] ));
		
		// Passwort ändern funktionalität
		if(isset($_POST['submitPW'])){
			//post data auslesen
			$oldPW = $this->escape($_POST['oldPW']);
			$pw1 = $this->escape($_POST['pw1']);
			$pw2 = $this->escape($_POST['pw2']);

			$v = new Validator();
			// altes pw prüfen
			$query = new Query('function', 'check_password');
			$query->addParameters(array($user->username, $oldPW));
			$storage = call_user_func(array('Mysql', 'getInstance'), array());
			if($storage->query($query) == 0){
				$message .= 'Altes Passwort stimmt nicht! <br />';
			}else{
				// validate new pw
				if(!$v->isString($pw1, 6, 1024))
					$message .= 'Ungültiges Password! Bitte beachten Sie, dass das Password mindestens 6 Zeichen lang sein muss!<br />';
				if($pw1 != $pw2)
					$message .= 'Passwörter stimmen nicht überein!<br />';
				
				// alles ok
				if($message != ''){
					$message ='<div class="flash-error">'.$message.'</div>';
				}else{	
					// pw setzten und speichern
					$user->password = $pw1;
					$user->save();
					$message ='<div class="flash-success">Passwort erfolgreich geändert</div>';
				}
			}
		}
		
		// alle einträge des aktuellen Users auslesen
		
		$entries = Entry::model()->findAllByAttributes(array('id_user' => $user->id));
		$output = '';

		if($entries == null){
			$output = 'Leider keine Einträge vorhanden';
		}else{
			foreach($entries as $entry){
				$user = User::model()->findByPk($entry->id_user);
				$output .= '<div class="entries">';
				$output .= '<div class="actionIcons"><a href="index.php?action=deleteEntry&id='.$entry->id.'"><img src="./images/delete.png" /></a>&nbsp;';
				$output .= '<a href="index.php?action=editEntry&id='.$entry->id.'"><img src="./images/edit.png" /></a> </div>';
				$output .= '<p>'.$entry->time.'</p>';
				$output .=	'<p>'. $user->username . ' ('. $user->email .') </p>';
				$output .= '<p>'.$entry->message.'</p>';
				$output .= '</div>';
			}
		}


		$content = $this->html->getPage('profile');
		$content = str_replace('{{ENTRIES}}', $output, $content);
		$content = str_replace('{{MESSAGE}}', $message, $content);
		echo $this->html->buildPage('Profil', $content);
	}

	private function actionRegister(){
		$v = new Validator();
		$user = new User();
		$message = '';

		// leere Variablen, damit kein Error ( da Werte wieder eingefüllt bei Error)
		$username = '';
		$email = '';
		$vorname = '';
		$nachname = '';
		$pw1 = '';
		$pw2 = '';


		if(isset($_POST['submitRegister'])){
			//post data auslesen
			$username = $this->escape($_POST['username']);
			$email = $this->escape($_POST['email']);
			$vorname = $this->escape($_POST['vorname']);
			$nachname = $this->escape($_POST['nachname']);
			$pw1 = $this->escape($_POST['passwort1']);
			$pw2 = $this->escape($_POST['passwort2']);

			// alle Felder validieren
			if(!$v->isString($username, 1, 1024))
				$message .= 'Ungültiger Username!<br />';
			if( $email != NULL && !$v->isEmail($email))
				$message .= 'Ungültige Email-Adresse! <br />';
			if(!$v->isString($vorname, 1, 1024))
				$message .= 'Ungültiger Vorname!<br />';
			if(!$v->isString($nachname, 1, 1024))
				$message .= 'Ungültiger Nachname!<br />';
			if(!$v->isString($pw1, 6, 1024))
				$message .= 'Ungültiges Password! Bitte beachten Sie, dass das Password mindestens 6 Zeichen lang sein muss!<br />';
			if($pw1 != $pw2)
				$message .= 'Passwörter stimmen nicht überein!<br />';
			// überprüfen ob der Username schon vergeben ist
			if(User::model()->findByAttributes(array('username' => $username )))
				$message .= 'Username schon vergeben';

			// alles ok
			if($message == ''){
				// user in DB speichern
				$arg = array('username' => $username, 'email'=> $email, 'vorname' => $vorname, 'nachname' => $nachname);
				$user->populate($arg);
				$user->save();
				$user->password = $pw1;
				$user->save();

				// Nachricht anzeigen und Home Seite laden
				$message .= '<div class="flash-success">Benutzerkonto erfolgreich registriert<br /></div>';
				$_SESSION['user']['username'] = $username;
				$this->actionIndex($message);
				return;
			}else{
				$message = '<div class="flash-error">' . $message. ' </div>';
			}		
		}
		$content = $this->html->getPage('register');
		$search =  array('{{username}}', '{{email}}', '{{vorname}}', '{{nachname}}', '{{pw1}}', '{{pw2}}');
		$replace = array($username, $email, $vorname, $nachname, $pw1, $pw1);
		$content = str_replace($search, $replace, $content);
		$content = str_replace('{{ERROR_MESSAGE}}', $message, $content);
		echo $this->html->buildPage('Registrieren', $content);
	}

	private function actionLogin(){
		$message = '';

		if(isset($_POST['submitLogin'])){
			//post data vorhanden?
			if(!isset($_POST['username']) || !isset($_POST['passwort'])){
				$message .= 'Bitte geben Sie ihren Usernamen und ihr Passwort ein!';
			}else{
				//post data auslesen
				$username = strtoupper($this->escape($_POST['username']));
				$pw = $this->escape($_POST['passwort']);
				
				$user = User::model()->findByAttributes(array('username' => $username ));
				// existiert der User in der Datenbank?
				if($user !== null){
					// passwort prüfen 
					$query = new Query('function', 'check_password');
					$query->addParameters(array($username, $pw));
					$storage = call_user_func(array('Mysql', 'getInstance'), array());
					if($storage->query($query) == 0){
						$message .= 'Benutzername oder Kennwort falsch';
					}else{
						$_SESSION['user']['username'] = $username;
						$message .= '<div class="flash-success">Erfolgreich eingeloggt<br /></div>';
						$this->actionIndex($message);
						return;
					}
				}else{
					$message .= 'Username existiert nicht.<br /> Falls Sie noch kein Benutzerkonto besitzten, dann registrieren Sie sich <a href="">hier.</a>';
				}
			}
		}

		if($message !=''){
			$message = '<div class="flash-error">' . $message. ' </div>';
		}else{
			$message = '<div class="flash-notice"> 
						Melden Sie sich hier mit ihrem Benutzerkonto an. <br />
						Sie haben noch kein Konto? Dann registrieren Sie sich 
						<a href="index.php?action=register"> hier</a>. </div>';
		}

		$content = $this->html->getPage('login');
		$content = str_replace('{{MESSAGE}}', $message, $content);
		echo $this->html->buildPage('Login', $content);
	}

	public function actionAbout(){
		$content = $this->html->getPage('about');
		echo $this->html->buildPage('Über mich', $content);
	}

	public function actionLogout(){
		if(isset($_SESSION['user']['username']) ){
			session_unset($_SESSION['user']['username']);
			$message = '<div class="flash-success">Erfolgreich ausgeloggt<br /></div>';
			$this->actionIndex($message);
			return;
		}		
	}

	private function escape($input){
		$escaped = htmlspecialchars(trim($input));
		return $escaped;
	}

}


?>
