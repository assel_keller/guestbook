<?php
/**
 * Copyright (C) 2013 Luginbühl Timon, Müller Lukas, Swisscom AG
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * For more informations see the license file or see <http://www.gnu.org/licenses/>.
 */

/**
 * ClassName: Validator
 * Inherits: Nothing
 *
 * Description:
 * This class provides functions to validate input.
 * The return value is alway false if input was not valid.
 */
Class Validator{

	/**
	 * Function: isString
	 *
	 * Description:
	 * Checks if the input is a string, if specified it also checks the length.
	 * @param $input the input which you want to validate
	 * @param $minlength the minimum length which the string should be long ( optional)
	 * @param $maxlength the maximum length which the string should be long ( optional)
	 * @return (bol) If the input is valid string
	 */
	public function isString($input, $minlength=0, $maxlength=null){
		$valid = 0;
		if($input !== null && is_string($input)){
			$valid ++;
		}

		// check max length
		if($maxlength !== null && strlen($input) <= $maxlength){
			$valid ++;
		}elseif($maxlength === null){
			$valid ++;
		}

		// check min length
		if($minlength !== null && strlen($input) >= $minlength){
			$valid ++;
		}elseif($minlength === null){
			$valid ++;
		}

		// if everthing is fine return true
		if($valid === 3)
			return true;
		return false;
	}

	/**
	 * Function: isEmail
	 *
	 * Description:
	 * Checks if the input is valid E-Mail Address.
	 *
	 * @param $input the input which you want to validate
	 * @return (bol) If the given email is valid
	 */
	public function isEmail($input){
		if (filter_var($input, FILTER_VALIDATE_EMAIL))
			return true;
		return false;
	}
}
?>
